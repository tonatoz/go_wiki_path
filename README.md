### WTF? ###

Program search terms chain between begin and end term at wikipedia (ru.wikipedia.org).

### How to use? ###

Flags:

* -b <begining term>
* -e <ending term>

### Example ###

```
#!Go

> .\wiki_path.exe -b SAP -e Go

!!!!!!!!!! Found: /wiki/Go !!!!!!!!!!!!!!!!!!!
parent: /wiki/KML
parent: /wiki/Германия
parent: /wiki/SAP
```

### Compiling ###
```
#!Go
> go build .\wiki_path.go
```